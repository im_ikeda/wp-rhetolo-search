=== RHETOLO Search Plugin ===
Contributors: picolabs
Tags: rhetolo,search
Requires at least: 3.1
Tested up to: 3.4
Stable tag: 1.0.0

Plug-in that Add a search feature using a RHETOLO search engine.

== Description ==

Short code to display the search result for the search form and RHETOLO code is added.

== Installation ==

1. Upload the `rhetolo-search` folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the Plugins menu in WordPress.
3. Write a short code "[rhsearch_form]" to where you want to display the RHEOTLO Search form.
4. Make a page with "rh-search" slug, and write a short code "[rhsearch_result]" to where you want to display the RHETOLO Search results.

5. If the location does not work if the short code, use do_shortcode () directly.
