<?php
class RhetoloSearch_Admin {
	public	$core;
	public	$hook;

	public function __construct(&$core) {
		$this->core = &$core;

		add_action('admin_init', array($this, 'initialize'));
		add_action('admin_menu', array($this, 'regist_menu'));
		add_action('current_screen', array($this, 'settings_screen'));
	}

	public function initialize() {
	}

	public function regist_menu() {
	}


}
