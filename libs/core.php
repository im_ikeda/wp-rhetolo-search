<?php
class RhetoloSearch {
	public	$cache;
	
	public	$admin;
	private	$storage;
	public	$template;
	public	$session;

	public	$defaults = array();

	public	$base_dir;

	public function __construct() {
		$this->base_dir = dirname(dirname(__FILE__));

		$this->load_default_options();

//		require_once(RHS_BASE_DIR .'/cache.php');
		require_once($this->base_dir .'/libs/template.php');
		require_once($this->base_dir .'/libs/session.php');
		require_once($this->base_dir .'/libs/api.php');
		require_once($this->base_dir .'/libs/util.php');

//		register_activation_hook(__FILE__, array($this, 'activate'));

		$this->template = new RhetoloSearch_Template($this);

/* とりあえずナシで
		if (is_admin()) {
			require_once(RHS_BASE_DIR .'/core/admin.php');
			$this->admin = new RhetoloSearch_Admin($this);
		}
*/
		add_action('init', array($this, 'initialize'));
		add_filter('wp_headers', array($this, 'start'));
	}

	private function load_default_options() {
		$this->defaults = array(
			'default_server'	=> '81',
			'page_slag'			=> 'rh-search',
			'shortcode_form'	=> 'rhsearch_form',
			'shortcode_result'	=> 'rhsearch_result',
		);
	}

	public function set_option($options, $value = NULL) {
		$current_options = $this->get_option();

		if (!is_array($options)) {
			if (isset($value)) {
				$options = array($options => $value);
			}
			else {
				return FALSE;
			}
		}

		$new_options = array_merge($current_options, $options);
		update_option(RHS_NAME, $new_options);
	}

	public function get_option($option = NULL) {
		$options = (array)get_option(RHS_NAME, array());
		$options = array_merge($this->defaults, $options);

		if (is_null($option)) return $options;

		$option_path = array();
		$parsed_option = array();

		wp_parse_str($option, $parsed_option);
		$option_path = $this->array_flatten($parsed_option);

		$current = $options;
		foreach ($option_path as $option_part) {
			if (!is_array($current) || !isset($current[$option_part]))
				return NULL;
			$current = $current[$option_part];
		}
		return $current;
	}

	private function array_flatten($array, $given = array()) {
		foreach ($array as $key => $val) {
			$given[] = $key;
			if (is_array($val)) {
				$given = $this->array_flatten($val, $given);
			}
		}
		return $given;
	}

	//------------
	public function initialize() {
		$this->session = new RhetoloSearch_Session();

		load_plugin_textdomain('rhetolo-search', FALSE,
										'rhetolo-search/lang');

		// Adding short code handler
		add_shortcode($this->get_option('shortcode_form'),
						array($this, 'handler_shortcode_form'));
		add_shortcode($this->get_option('shortcode_result'),
						array($this, 'handler_shortcode_result'));
	}

	public function start($headers) {
		$this->session = new RhetoloSearch_Session();
		return $headers;
	}

	//------------
	public function handler_shortcode_form($attrs) {
//		wp_enqueue_script('jquery');
//		wp_enqueue_script('rhsearch', RHS_BASE_DIR .'/js/rhsearch.js');
		wp_enqueue_style('rhsearch',
				RHS_BASE_URL .'common/css/rhsearch.css');

		$server = $this->get_option('server');
		$page_slag = $this->get_option('page_slag');
		if (!empty($attrs) && is_array($attrs)) {
			extract(shortcode_attrs(array(
				'server'	=> $server,
				'page_slag'	=> $page_slag,
			), $attrs));
		}

		$server = array_key_exists('rhss', $_REQUEST) ?
									$_REQUEST['rhss'] :
									$server;
		$rhcode = array_key_exists('rhsc', $_REQUEST) ?
									$_REQUEST['rhsc'] : NULL;
		$this->template->bind('server', $server)
					   ->bind('rhcode', $rhcode)
					   ->bind('page_slag', $page_slag);
		return $this->template->render('search_form');
	}

	public function handler_shortcode_result($attrs) {
		wp_enqueue_style('rhsearch',
				RHS_BASE_URL .'common/css/rhsearch.css');

		$page_slag = $this->get_option('page_slag');
		if (!empty($attrs) && is_array($attrs)) {
			extract(shortcode_attrs(array(
				'page_slag'	=> $page_slag,
			), $attrs));
		}

		$action = array_key_exists('rhsa', $_REQUEST) ?
									$_REQUEST['rhsa'] : 'search';

		$server = array_key_exists('rhss', $_REQUEST) ?
									$_REQUEST['rhss'] :
									$this->get_option('server');
		$rhcode = array_key_exists('rhsc', $_REQUEST) ?
									$_REQUEST['rhsc'] : NULL;

		if (empty($rhcode)) {
			return $this->template->render('error_rhcode_empty');
		}

		switch ($action) {
		case "link":
			// RHETOLO Link からの起動
			$hists = array_key_exists('rhsh', $_REQUEST) ?
										$_REQUEST['rhsh'] :
										NULL;
			if (!empty($hists)) {
				$history = $this->session->get('history');
				if (!is_array($history)) {
					$history = array($history);
				}

				$history = array_unique($history);
				$idx = array_search($hists, $history);
				if ($idx === FALSE) {
					array_push($history, $hists);
				}
				foreach ($history as $idx => $val) {
					if (empty($val)) {
						unset($history[$idx]);
					}
				}
				$this->session->set('history', $history);
			}
			$this->template->bind('history', $history);
			break;
		case "prev":
			$prevcode = "{$server}:{$rhcode}";
			// 履歴を戻っている
			$history = $this->session->get('history');
			if (!is_array($history)) {
				$history = array($history);
			}

			$idx = array_search($prevcode, $history);
			if ($idx !== FALSE) {
				unset($history[$idx]);
			}

			if (count($history) <= 0) {
				$this->session->delete('history');
			}
			else {
				foreach ($history as $idx => $val) {
					if (empty($val)) {
						unset($history[$idx]);
					}
				}
				$this->session->set('history', $history);
			}
			$this->template->bind('history', $history);
			break;
		default:
			// 通常検索
			$this->session->restart();
			break;
		}
		$this->template->bind('server', $server)
					   ->bind('rhcode', $rhcode)
					   ->bind('page_slag', $page_slag);

		$api = new RhetoloSearch_Api();
		try {
			$response = $api->find($rhcode, $server);
		} catch (Exception $e) {
			$errcode = $e->getStatus();
			$errmsg  = $e->getMessage();
			$this->template->bind('errcode', $errcode)
						   ->bind('errmsg', $errmsg);
			return $this->template->render('error_api');
		}

		if (!$response) {
			$status = $api->getStatus();
			switch ($status) {
			case RhetoloSearch_Const::NOTFOUND:
				$content = $this->template->render('error_notfound');
				break;
			default:
				$errcode = $e->getStatus();
				$errmsg  = $e->getMessage();
				$this->template->bind('errcode', $errcode)
							   ->bind('errmsg', $errmsg);
				$content = $this->template->render('error_api');
				break;
			}
			return $content;
		}

		$histories = $this->session->get('history');
		if ($histories) {
			$this->template->bind('history', $histories);
		}

		$this->template->bind('response', $response);
		$this->session->write();

		return $this->template->render('search_result');
	}

	//------------
	public function activate() {
		if (!get_option('rhetolo-search_version')) {
			// new install
			add_option('rhetolo-search_version', RHS_VERSION);
		}
	}

	public function deactivate() {
	}
}
