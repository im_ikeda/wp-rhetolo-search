<?php
class RhetoloSearch_Const {
	const		OK				= 200;
	const		ACCEPTED		= 202;
	const		FORBIDDEN		= 403;
	const		NOTFOUND		= 404;
	const		NOTACCEPTABLE	= 406;
	const		ERROR			= 999;

	const		USER_AGENT		= 'RhetoloWebSearch/1.0.0; 81;';
	const		DEFAULT_SERVER	= '81';
	const		DEFAULT_ICONURL	= 'common/img/siteid.png';
}
