<?php
class RhetoloSearch_Driver {
	protected	$apis = array(
		'81'	=> array(
			'name'	=> 'RHETOLO Japan',
			'url'	=> 'http://81api.rhetolo.com/v2/',
		),
		'82'	=> array(
			'name'	=> 'RHETOLO Korea',
			'url'	=> 'http://82api.rhetolo.com/v2/',
		),
		'00'	=> array(
			'name'	=> 'RHETOLO Develop',
			'url'	=> 'http://00api.rhetolo.com/v2/',
		),
	);
	protected	$server = NULL;
	protected	$api_base = NULL;

	protected	$parser = NULL;

	public		$status	= NULL;
	public		$message= NULL;
	public		$response_history = array();

	public function getStatus() { return $this->status; }
	public function getMessage() { return $this->message; }
	public function getHistoryCount() {
		return count($this->response_history);
	}
	public function getHistories() {
		return $this->response_history;
	}

	public function __construct($server = NULL) {
		require_once(dirname(__FILE__) .'/const.php');
		require_once(dirname(__FILE__) .'/parser.php');

		$this->server = RhetoloSearch_Const::DEFAULT_SERVER;
		$this->api_base = $this->apis[$this->server]['url'];

		if ($server) $this->setServer($server);
	}

	public function __get($name) {
		if (property_exists($this, $name)) {
			return $this->$name;
		}
		return FALSE;
	}

	public function setServer($server) {
		if (array_key_exists($server, $this->apis)) {
			$this->api_base = $this->apis[$server]['url'];
			$this->server = $server;
		}
		return FALSE;
	}

	public function getUserAgent($ua = NULL) {
		if ($ua == NULL && !empty($_SERVER['HTTP_USER_AGENT'])) {
			$ua = $_SERVER['HTTP_USER_AGENT'];
		}

		$useragent = RhetoloSearch_Const::USER_AGENT;
		if (!empty($ua) && $ua != '-') {
			$useragent .= ' '. $ua;
		}
		return $useragent;
	}

	protected function apiurl($detail) {
		return $this->api_base . $detail;
	}

	public function query($rhcode, $server = NULL) {
		$this->message = '';
		if (empty($rhcode)) {
			$this->status = RhetoloSearch_Const::ERROR;
			$this->message = 'RHETOLO CODE not set.';
			throw new Exception($this->message, $this->status);
			return FALSE;
		}
		if (!isset($server)) { $server = $this->server; }

		$codes = RhetoloSearch_Parser::splitRHCode($rhcode);
		$url = $this->apiurl('search.xml/'.
								$codes['rcode'] .'*'. $codes['pcode']);
		return $this->_exec($url);
	}


	public function fixer($source) {
		$parser = RhetoloSearch_Parser::instance();
		$fixerurl = $parser->getfixer($source);
		if (empty($fixerurl)) {
			$this->status = RhetoloSearch_Const::ERROR;
			$this->message = 'fixerurl is empty.';
			throw new Exception($this->message, $this->status);
			return FALSE;
		}
		$this->response_history[] = $source;
		return $this->_exec($fixerurl);
	}

	public function getResponseHistory($idx = NULL) {
		if ($idx === NULL) return $this->response_history;
		return $this->response_history[$idx];
	}

	private function _exec($url) {
		$options = array(
//			CURLOPT_HTTPHEADER		=> array(
//			),
			CURLOPT_USERAGENT		=> $this->getUserAgent(),
			CURLOPT_RETURNTRANSFER	=> TRUE,
			CURLOPT_HEADER			=> FALSE,
			CURLOPT_SSL_VERIFYPEER	=> FALSE,
			CURLOPT_FOLLOWLOCATION	=> TRUE,
			CURLOPT_MAXREDIRS		=> 5,
		);


		$ch = curl_init($url);
		if (!curl_setopt_array($ch, $options)) {
			$this->status = RhetoloSearch_Const::ERROR;
			$this->message = 'Failed to set CURL options.';
			throw new Exception('Failed to set CURL options.');
			return FALSE;
		}

		$contents = curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		switch ($status) {
		case	200:		// OK
			break;
		case	202:		// ACCEPTED
			return $this->fixer($contents);
		case	404:
			$this->status = RhetoloSearch_Const::NOTFOUND;
			$this->message = '';
			return FALSE;
		case	403:
			$this->status = RhetoloSearch_Const::FORBIDDEN;
			$this->message = '';
			return FALSE;
		case	406:
			$this->status = RhetoloSearch_Const::NOTACCEPTABLE;
			$this->message = '';
			return FALSE;
		default:
			$this->status = RhetoloSearch_Const::ERROR;
			$this->message = '';
			return FALSE;
		}

		if ($contents == FALSE || empty($contents)) {
			$this->status = RhetoloSearch_Const::ERROR;
			$this->message = curl_error($ch);
			return FALSE;
		}
		curl_close($ch);

		return $contents;
	}
}
