<?php
class RhetoloSearch_Session {
	protected	$_prefix = 'RHS_';
	protected	$_name = 'RhetoloSearch';
	protected	$_lifetime = 0;
	protected	$_data = array();
	protected	$_destroyed = FALSE;

	public function __construct($config = NULL, $id = NULL) {
		if (isset($config['name']))
			$this->_name = (string)$config['name'];
		if (isset($config['prefix']))
			$this->_prefix = (string)$config['prefix'];
		if (isset($config['lifetime']))
			$this->_lifetime = intval($config['lifetime']);

		register_shutdown_function(array($this, 'write'));

		$this->initialize($id);
	}

	protected function initialize($id = NULL) {
		session_set_cookie_params($this->_lifetime);
		session_cache_limiter(FALSE);
		session_name($this->_name);

		if (isset($id)) {
			session_id($id);
		}

		$this->start();
	}

	public function start() {
//		if (!headers_sent()) {
			$status = session_start();
			$this->_data =& $_SESSION;
			return $status;
//		}
//		return FALSE;
	}

	public function regenerate() {
		session_regenerate_id();
		return session_id();
	}

	public function &as_array() {
		return $this->_data;
	}

	public function id() {
		return session_id();
	}

	public function name() {
		return $this->_name;
	}

	public function genkey($key) {
		return $this->_prefix . sha1($key);
	}

	public function get($key, $default = NULL) {
		$newkey = $this->genkey($key);
		return array_key_exists($newkey, $this->_data) ?
								$this->_data[$newkey] : $default;
	}

	public function pop($key, $default = NULL) {
		$value = $this->get($key, $default);

		$newkey = $this->genkey($key);
		unset($this->_data[$newkey]);

		return $value;
	}

	public function set($key, $value) {
		$newkey = $this->genkey($key);
		$this->_data[$newkey] = $value;

		return $this;
	}

	public function bind($key, &$value) {
		$newkey = $this->genkey($key);
		$this->_data[$newkey] = &$value;

		return $this;
	}

	public function delete($key) {
		$args = func_get_args();
		foreach ($args as $key) {
			$newkey = $this->genkey($key);
			unset($this->_data[$newkey]);
		}
		return $this;
	}

	public function write() {
		if (headers_sent() || $this->_destroyed) {
			return FALSE;
		}
		$this->set('last_active', time());
		session_write_close();
		return TRUE;
	}

	public function destroy() {
		if (!$this->_destroyed) {
			if ($this->_destroyed = session_destroy()) {
				unset($this->_data);
				$this->_data = array();
			}
		}
		return $this->_destroyed;
	}

	public function restart() {
		if (!$this->_destroyed) {
			$this->destroy();
		}
		$this->_destroyed = FALSE;

		return $this->start();
	}
}
