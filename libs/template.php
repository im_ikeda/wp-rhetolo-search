<?php

class RhetoloSearch_Template {
	public	$core;
	public	$template_dir;
	private $_binds = array();

	public function __construct(&$core) {
		$this->core = &$core;
		$this->template_dir = $core->base_dir .'/templates';
	}

	public function render($templ, $params = array()) {
		extract($this->_binds);
		extract($params);

		ob_start();
		include($this->template_dir .'/'. $templ .'.php');
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
	}

	public function show($templ, $params = array()) {
		echo $this->render($templ, $params);
	}

	public function bind($name, &$val) {
		$this->_binds[$name] = &$val;
		return $this;
	}
}
