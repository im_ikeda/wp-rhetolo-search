<?php

class RhetoloSearch_Parser {
	public static	$parser = NULL;
	public $xmlparser = NULL;

	public static function instance() {
		if (!isset(self::$parser)) {
			self::$parser = new RhetoloSearch_Parser();
		}
		return self::$parser;
	}

	public function __construct() {
		require_once(dirname(__FILE__) .'/const.php');
		if (!class_exists('simple_html_dom')) {
			require_once(dirname(__FILE__) .'/simple_html_dom.php');
		}

		if ($this->xmlparser === NULL) {
			$this->xmlparser = new simple_html_dom();
		}
		return $this;
	}

	public static function splitRHCode($rhcode) {
		$m = explode('*', $rhcode);
		return array(
			'rcode'	=> $m[0],
			'pcode'	=> (count($m) < 2 ? '0' : $m[1]),
		);
	}

	public function exec($sources) {
		$this->xmlparser->clear();
		$this->xmlparser->load($sources);

		$response = array(
			'request'	=>
					$this->parse_request($this->findnode('request')),
			'name'		=> NULL,
			'icon'		=> NULL,
			'rating'	=> NULL,
		);

		$status = $response['request']['status'];
		switch ($status) {
		case	RhetoloSearch_Const::OK:		// OK
			break;
		default:
			return $response;
		}

		$codeinfo = $this->parse_codeinfo($this->xmlparser);
		$response['name']	= $codeinfo['name'];
		$response['icon']	= $codeinfo['icon'];
		$response['rating']	= $codeinfo['rating'];
		unset($codeinfo);

		$response['services'] =
					$this->parse_services($this->findnode('services'));
		return $response;
	}

	public function parse_request($node) {
		if (!is_object($node)) {
			$this->xmlparser->load($node);
			$node = $this->findnode('request');
		}
		return array(
			'url'	=> $this->findtext('url', $node),
			'server'=> $this->findtext('server', $node),
			'status'=> $this->findtext('status', $node),
			'rcode'	=> $this->findtext('rcode', $node),
			'pcode'	=> $this->findtext('pcode', $node),
		);
	}

	public function parse_codeinfo($node) {
		if (!is_object($node)) {
			$this->xmlparser->load($node);
			$node = $this->xmlparser;
		}
		return array(
			'name'	=> $this->findtext('name', $node),
			'icon'	=> $this->findtext('icon', $node),
			'rating'=> $this->findtext('rating', $node),
		);
	}

	public function parse_services($node) {
		if (!is_object($node)) {
			$this->xmlparser->load($node);
			$node = $this->findnode('services');
		}

		$services = array();
		foreach ($node->find('service') as $idx => $service) {
			$attr = array(
				'id'			=> (string)$service->id,
				'type'			=> (string)$service->type,
				'permission'	=> (string)$service->permission,
				'order'			=> (string)$service->order,
			);

			$resource_type = $this->findtext('resource-type', $service);
			$services[$idx] = array(
				'attr'	=> $attr,
				'label'	=> $this->findtext('label', $service),
				'icon'	=> $this->findtext('icon', $service),
				'description'
						=> $this->findtext('description', $service),
				'rating'=> $this->findtext('rating', $service),
				'resource-type'	=> $resource_type,
			);
			if (empty($attr['permission']) ||
				intval($attr['permission']) <= 0) {
				$services[$idx]['resource'] =
						$this->parse_resource($resource_type,
								$this->findnode('resource', $service));
			}
		}
		return $services;
	}

	public function parse_resource($type, $resource) {
		switch ($type) {
		case 1:		// URL
		case 3:		// MOVIE
		case 6:		// SHOP
		case 7:		// PAY
			return array(
				'url'	=> $this->findtext('url', $resource),
			);
		case 2:		// MAP
			$location = $this->findnode('location', $resource);
			return array(
				'latitude'	=> $this->findtext('latitude', $location),
				'longitude'	=> $this->findtext('longitude', $location),
			);
		case 4:		// MAIL
			$email = $this->findnode('email', $resource);
			return array(
				'to'		=> $this->findtext('to', $email),
				'subject'	=> $this->findtext('subject', $email),
				'body'		=> $this->findtext('body', $email),
			);
		case 5:		// TEL
			return array(
				'telephone'	=> $this->findtext('number', $resource),
			);
		case 8:		// RLINK
			return array(
				'rlink'	=> $this->findtext('rlink', $resource),
				'rlink_server'
						=> $this->findtext('rlink_server', $resource),
			);
		}
	}

	public function findnode($name, $parent = NULL) {
		if (!$parent) $parent = $this->xmlparser;
		return $parent->find($name, 0);
	}

	public function findtext($name, $parent = NULL) {
		if (!$parent) $parent = $this->xmlparser;

		$found = $parent->find($name, 0);
		if (!$found) return $found;
		return (string)$found->xmltext();
	}

	public function getfixer($source) {
		$this->xmlparser->clear();
		$this->xmlparser->load($source);
		return $this->findtext('fixer');
	}

	public static function a2n($alphacode) {
		$alphas = array(
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
				'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
				'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		);
		$nums = array(
				'21', '22', '23', '31', '32', '33', '41', '42', '43',
				'51', '52', '53', '61', '62', '63', '71', '72', '73',
				'74', '81', '82', '83', '91', '92', '93', '94',
		);
		return str_ireplace($alphas, $nums, strtoupper($alphacode));
	}
}
