<?php
class RhetoloSearch_Util {
	// 携帯・スマフォ系と判断するUSER_AGENT文字列
	/*@(#)20121016 add ikeda ( */
	protected static $_mobile_agents = array(
		'iphone'         => 'iPhone',
		'ipad'           => 'iPad',
		'ipod'           => 'iPod',
		'android'        => 'Android',
		'windows phone'  => 'Windows Phone',
		'docomo'         => 'DoCoMo',
		'kddi'           => 'KDDI',
		'ddipocket'      => 'DDI-POCKET',
		'up.browser'     => 'UP.Browser',
		'j-phone'        => 'J-PHONE',
		'vodafone'       => 'Vodafone',
		'softbank'       => 'Softbank',
		'mobileexplorer' => 'Mobile Explorer',
		'openwave'       => 'Open Wave',
		'opera mini'     => 'Opera Mini',
		'operamini'      => 'Opera Mini',
		'elaine'         => 'Palm',
		'palmsource'     => 'Palm',
		'digital paths'  => 'Palm',
		'avantgo'        => 'Avantgo',
		'xiino'          => 'Xiino',
		'palmscape'      => 'Palmscape',
		'nokia'          => 'Nokia',
		'ericsson'       => 'Ericsson',
		'blackBerry'     => 'BlackBerry',
		'motorola'       => 'Motorola',
	);

	// 携帯orスマフォ判断
	public static function is_mobile($agent = NULL) {
		global	$_mobile_agents;
		static	$_is_mobile	= NULL;

		if ($_is_mobile !== NULL) return $_is_mobile;
		if ($agent == NULL) {
			$agent = $_SERVER['HTTP_USER_AGENT'];
		}

		foreach (self::$_mobile_agents as $search => $name) {
			if (stripos($agent, $search) !== FALSE) {
				$_is_mobile = $name;
				return $_is_mobile;
			}
		}
		$_is_mobile = FALSE;
		return $_is_mobile;
	}

	function is_smartphone($agent = NULL) {
		global	$_mobile_agents;
		static	$_is_smartphone	= NULL;

		if ($_is_smartphone !== NULL) return $_is_smartphone;
		$mobile = self::is_mobile($agent);

		switch (strtolower($mobile)) {
		case 'iphone':
		case 'ipad':
		case 'ipod':
		case 'android':
		case 'windows phone':
		case 'blackberry':
			$_is_smartphone = TRUE;
			break;
		default:
			$_is_smartphone = FALSE;
			break;
		}
		return $_is_smartphone;
	}
	/*@(#)20121016 add ikeda )
	　使用するときは
	　if (is_mobile()) { // 携帯 }
	　if (is_smartphone()) { // 携帯かつスマフォ }
	*/
}
