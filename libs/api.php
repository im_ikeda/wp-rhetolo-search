<?php
class RhetoloSearch_API {
	public	$server;
	public	$rhcode;

	public function __construct($server = NULL) {
		$this->initialize();
		$this->server($server);
	}

	public function initialize() {
		require_once(dirname(__FILE__) .'/driver.php');
		require_once(dirname(__FILE__) .'/parser.php');
	}

	public function server($server = NULL) {
		if (isset($server)) {
			$this->server = $server;
			return $this;
		}
		return $this->server;
	}

	public function rhetolocode($rhcode = NULL) {
		if (isset($rhcode)) {
			$this->rhcode = $rhcode;
			return $this;
		}
		return $this->rhcode;
	}

	public $last_status = NULL;
	public $last_message = NULL;
	public function find($rhcode = NULL, $server = NULL) {
		if (!isset($server)) $server = $this->server();
		if (!isset($rhcode)) $rhcode = $this->rhetolocode();

		$rhcode = preg_replace("/[^:0-9a-zA-Z*]/", "", $rhcode);
		if (strpos($rhcode, ':') !== FALSE) {
			list($server, $rhcode) = explode(':', $rhcode);
		}
		if (empty($rhcode)) {
			throw new Exception('RHETOLO Code is undefined!');
		}

		$driver = new RhetoloSearch_Driver($server);
		try {
			$contents = $driver->query($rhcode);
		} catch (Exception $e) {
			$this->last_status = $driver->getStatus();
			$this->last_message = $driver->getMessage();

			throw $e; 
		}

		$this->last_status = $driver->getStatus();
		$this->last_message = $driver->getMessage();
		if ($contents === FALSE) {
			switch ($this->last_status) {
			case RhetoloSearch_Const::NOTFOUND:
				return FALSE;
			default:
				throw new Exception($this->last_message,
									$this->last_status);
				break;
			}
		}

		$parser = RhetoloSearch_Parser::instance();
		$response = $parser->exec($contents);
		return $response;
	}

	public function getStatus() { return $this->last_status; }
	public function getMessage() { return $this->last_message; }
}
