<?php if (RhetoloSearch_Util::is_smartphone()) : ?>
<div class="openSpAppli">
<a href="rhetolo://rhetolo.com/<?php echo $server; ?>/<?php echo $response['request']['rcode'], '*', $response['request']['pcode']; ?>"><?php _e('Open this code in the RHETOLO App.', 'rhetolo-search'); ?></a>
</div>
<?php endif; ?>

<?php
$plugin_url = RHS_BASE_URL;
$reurl = home_url() .'/'. $page_slag;
if (!empty($history)) {
	if (!is_array($history)) {
		$history = array($history);
	}

	$histitems = array();
	if (count($history) > 5) {
		$hists = array_slice($history, -5);
		$histitems[] = "&nbsp;";
	}
	else {
		$hists = $history;
		$histitems[] = "<a href='". home_url() ."'>HOME</a>";
	}

	foreach ($hists as $idx => $hist) {
		list($server, $code) = explode(':', $hist);
		$histitems[] =
			"<a href='{$reurl}?". http_build_query(array(
										'rhss'	=> $server,
										'rhsc'	=> $code,
										'rhsa'	=> 'prev',
									), NULL, '&').
			"'>R*) {$code}</a>";
	}
	echo '<div class="rlinkHistory">',
		 implode(' &raquo; ', $histitems),
		'</div>';
}
?>

<div class="detailRhcode clrFix">
<div class="siteicon"><img src="<?php
	echo empty($response['icon']) ?
					$plugin_url .'/common/icon/rhmark.png' :
					$response['icon']; ?>" alt="SITE ICON"></div>
<div class="permission"></div>
<div class="title">
<h1><?php echo $response['name']; ?></h1>
<h2><?php echo $response['request']['rcode'], '*', $response['request']['pcode']; ?></h2>
<!-- test -->
<br /><br />
<a href="http://rhetolo.com/<?php echo $server; ?>/<?php echo $response['request']['rcode'], '*', $response['request']['pcode']; ?>">アプリで開くリンク　その1</a>
<br />
<a href="rhetolo://rhetolo.com/<?php echo $server; ?>/<?php echo $response['request']['rcode'], '*', $response['request']['pcode']; ?>">アプリで開くリンク　その2</a>
<!-- test -->
</div>
</div>

<div class="menuItemList">
<ul>
<?php
$services = $response['services'];
foreach ($services as $idx => $service) {
	if (empty($service['icon'])) {
		$service['icon'] = $plugin_url .'/common/icon/'.
								$service['resource-type'] .'.png';
	}

	$itemcontent = '<div class="menuicon">'.
				'<img src="'. $service['icon'] .'" '.
					'alt="MENU ICON" width="64" height="64"></div>';

	$target = '';
	$label = $service['label'];
	$prop = array();
	if (intval($service['attr']['permission']) == 0) {
		$resource = @$service['resource'];
		switch ($service['resource-type']) {
		case 1:	// URL
		case 3:	// MOVIE
		case 6:	// SHOP
		case 7:	// PAY
			$target = $resource['url'];
			$prop = array('target' => '_blank');
			break;
		case 2:	// MAP
			$lat = $resource['latitude'];
			$lng = $resource['longitude'];
			$target = 'http://maps.google.co.jp/maps?'.
						"q={$lat},{$lng}({$label})&".
						"ll={$lat},{$lng}&z=18";
			$prop = array('target' => '_blank');
			break;
		case 4:	// MAIL
			$target = "mailto:{$resource['to']}";
			break;
		case 5:	// TEL
			if (function_exists('is_smartphone') && is_smartphone()) {
				$target = "tel:{$resource['telephone']}";
			}
			else {
				$target = '';
				$label .= '<br /><span class="notes gray fss">'.
						__('TELEPHONE Item is not available.', 'rhetolo-search').
						'</span>';
			}
			break;
		case 8:	// RLINK
			$target = $reurl ."?".  http_build_query(array(
								'rhss'	=> $resource['rlink_server'],
								'rhsc'	=> $resource['rlink'],
								'rhsa'	=> 'link',
								'rhsh'	=> $server .':'. $rhcode,
							), NULL, '&');
			break;
		}
		$itemcontent .= '<div class="permission"></div>';
	}
	else {
		$itemcontent .= '<div class="permission">'.
			"<img src='{$plugin_url}/common/icon/key.png' ".
				"alt='PASSWORD'></div>";
		$label .= '<br /><span class="notes gray fss">'.
				__('Item with authentication is not available.', 'rhetolo-search').
				'</span>';
	}
	$itemcontent .= "<div class='title'>{$label}</div>";

	if (!empty($target)) {
		$props = "";
		 if (!preg_match('/^(mailto|tel):/', $target)) {
			if (isset($prop['class'])) {
				$prop['class'] .= ' menuSelect';
			}
			else {
				$prop['class'] = 'menuSelect';
			}

			foreach ($prop as $key => $val) {
				$props .= " {$key}='{$val}'";
			}
		}
		echo '<li class="clrFix">',
			 "<a href='{$target}' class='menuSelect'{$props}>",
			 $itemcontent ,'</a></li>';
	}
	else {
		echo '<li class="clrFix">', $itemcontent, '</li>';
	}
}
?>
</ul>
</div>
