<section class="rh-search clrFix">
<form id="rhetolo-search-form" action="<?php echo home_url(); ?>/<?php echo $page_slag; ?>" method="GET" enctype="multipart/form-data">
<h1><?php _e('RHETOLO Code Search', 'rhetolo-search'); ?></h1>
<select class="rhsearch_country" name="rhss">
	<option class="jp" value="81" <?php if ($server == '81') echo 'selected'; ?>><?php _e('Japan', 'rhetolo-search'); ?></option>
	<option class="kr" value="82" <?php if ($server == '82') echo 'selected'; ?>><?php _e('Korea', 'rhetolo-search'); ?></option>
</select>
<input class="rhsearch_code" name="rhsc" value="<?php echo $rhcode; ?>" type="text"><input type="submit" value="<?php _e('Search', 'rhetolo-search'); ?>" class="submitBtn">
</form>
</section>
