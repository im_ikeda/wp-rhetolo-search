<?php
/*
Plugin Name: Rhetolo Search
Plugin URI: http://rhetolo.com/
Description: RHETOLO 検索を実装します。
Version: 0.0.1
Author: Takashi Ikeda
Author URI: http://rhetolo.com
Text Domain: rhetolo-search
*/

//----- Defines
if ( !defined('WP_CONTENT_URL') ){
	define('WP_CONTENT_URL', get_option('siteurl') .'/wp-content');
}
if ( !defined('WP_CONTENT_DIR') ){
	define('WP_CONTENT_DIR', ABSPATH .'wp-content');
}

define('RHS_NAME', 'rhetolo-search');
define('RHS_VERSION', '0.0.1');
define('RHS_BASE_DIR', WP_PLUGIN_DIR .'/'.
	str_replace(basename(__FILE__), "", plugin_basename(__FILE__)));
define('RHS_BASE_URL', WP_PLUGIN_URL .'/'.
	str_replace(basename(__FILE__), "", plugin_basename(__FILE__)));

require_once(RHS_BASE_DIR .'/libs/core.php');
$rhsearch = new RhetoloSearch();
